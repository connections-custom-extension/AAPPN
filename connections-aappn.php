<?php
/**
 * An extension for the Connections Business Directory plugin customizing it for the AAPPN website.
 *
 * @package   Connections AAPPN
 * @category  Extension
 * @author    Steven A. Zahm
 * @license   GPL-2.0+
 * @link      http://connections-pro.com
 * @copyright 2016 Steven A. Zahm
 *
 * @wordpress-plugin
 * Plugin Name:       Connections AAPPN
 * Plugin URI:        http://connections-pro.com
 * Description:       An extension for the Connections Business Directory plugin customizing it for the AAPPN website.
 * Version:           1.0
 * Author:            Steven A. Zahm
 * Author URI:        http://connections-pro.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       connections-aappn
 * Domain Path:       /languages
 */

if ( ! class_exists( 'CN_AAPPN' ) ) :

	final class Connections_AAPPN {

		/**
		 * @since 1.0
		 */
		const VERSION = '1.0';

		/**
		 * Stores the instance of this class.
		 *
		 * @var $instance Connections_AAPPN
		 *
		 * @access private
		 * @static
		 * @since  1.0
		 */
		private static $instance;

		/**
		 * A dummy constructor to prevent the class from being loaded more than once.
		 *
		 * @access public
		 * @since  1.0
		 */
		public function __construct() { /* Do nothing here */ }

		/**
		 * The main plugin instance.
		 *
		 * @access  private
		 * @static
		 * @since   1.0
		 *
		 * @return object self
		 */
		public static function instance() {

			if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Connections_AAPPN ) ) {

				self::$instance = new Connections_AAPPN;

				self::hooks();
			}

			return self::$instance;
		}

		private static function hooks() {

			// Register custom metaboxes and fields.
			add_action( 'cn_metabox', array( __CLASS__, 'registerMetaboxes' ) );

			// Register the custom fields CSV Import mapping options and processing callback.
			add_filter( 'cncsv_map_import_fields', array( __CLASS__, 'csvImportOptions' ) );
			add_action( 'cncsv_import_fields', array( __CLASS__, 'csvImportCustomFields' ), 10, 3 );

			// Register the custom fields CSV Export attributes and processing callback.
			add_filter( 'cn_csv_export_fields', array( __CLASS__, 'csvExportHeaderNames' ) );
			add_filter( 'cn_csv_export_fields_config', array( __CLASS__, 'csvExportFieldsConfig' ) );

			// Remove unused core field sets.
			add_action( 'admin_init', array( __CLASS__, 'removeMetaboxes' ) );

			// Allow only the individual entry type.
			add_filter( 'cn_admin_metabox_publish_atts', array( __CLASS__, 'entryTypes' ), 11 );

			// Register additional address types.
			add_filter( 'cn_address_options', array( __CLASS__, 'registerAddressTypes' ) );

			// Register additional phone number types.
			add_filter( 'cn_phone_options', array( __CLASS__, 'registerPhoneNumberTypes' ) );

			// Register additional email address types.
			add_filter( 'cn_email_options', array( __CLASS__, 'registerEmailAddressTypes' ) );

			// Only allow members to search by Modality.
			add_filter( 'cnmcs_category_search_options', array( __CLASS__, 'multipleCategorySearchOptions' ) );

			// Do not allow users without the `connections_view_unlisted` capability to select the "For AAPPN Use" data types.
			add_filter( 'cn_address_options', array( __CLASS__, 'removeAAPPNAddressOption' ) );
			add_filter( 'cn_phone_options', array( __CLASS__, 'removeAAPPNPhoneOption' ) );
			add_filter( 'cn_email_options', array( __CLASS__, 'removeAAPPNEmailOption' ) );

			// Remove the address, phone ane email option filters so the "For AAPPN Use" data types can be saved.
			add_action( 'cn_save-entry', array( __CLASS__, 'removeAAPPNFilters') );
			add_action( 'cn_update-entry', array( __CLASS__, 'removeAAPPNFilters') );
			add_action( 'cngfc_pre_process_feed', array( __CLASS__, 'removeAAPPNFilters') );

			// Prevent "For AAPPN Use" data from being displayed or edited by users.
			// Only users with the `connections_view_unlisted` capability and in the admin will be able to see these.
			add_filter( 'cn_address-pre_setup', array( __CLASS__, 'setAAPPNAddressUnlisted' ) );
			add_filter( 'cn_addresses', array( __CLASS__, 'removeAAPPNAddresses' ) );

			add_filter( 'cn_phone-pre_setup', array( __CLASS__, 'setAAPPNPhoneUnlisted' ) );
			add_filter( 'cn_phone_numbers', array( __CLASS__, 'removeAAPPNPhone' ) );

			add_filter( 'cn_email-pre_setup', array( __CLASS__, 'setAAPPNEmailUnlisted' ) );
			add_filter( 'cn_email_addresses', array( __CLASS__, 'removeAAPPNEmail' ) );

			// Register custom Content Blocks.
			add_filter( 'cn_content_blocks', array( __CLASS__, 'registerContentBlocks' ) );

			// Render custom Content Blocks.
			add_action( 'cn_entry_output_content-entry_field-primary', array( __CLASS__, 'renderPrimaryContentBlock' ), 10, 3 );
			add_action( 'cn_entry_output_content-entry_field-secondary', array( __CLASS__, 'renderSecondaryContentBlock' ), 10, 3 );

			add_action( 'cn_entry_output_content-entry_field-bio', array( __CLASS__, 'renderBioFieldContentBlock' ), 10, 3 );
			add_action( 'cn_entry_output_content-custom_field-preferred_name', array( __CLASS__, 'renderPreferredNameContentBlock' ), 10, 3 );
			add_action( 'cn_entry_output_content-categories-age_groups', array( __CLASS__, 'renderAgeGroupsContentBlock' ), 10, 3 );
			add_action( 'cn_entry_output_content-categories-issues_treated', array( __CLASS__, 'renderIssuesTreatedContentBlock' ), 10, 3 );
			add_action( 'cn_entry_output_content-categories-modality', array( __CLASS__, 'renderModalityContentBlock' ), 10, 3 );
			add_action( 'cn_entry_output_content-custom_field-accept_insurance', array( __CLASS__, 'renderAcceptInsuranceContentBlock' ), 10, 3 );
			add_action( 'cn_entry_output_content-custom_field-custom_work_setting', array( __CLASS__, 'renderWorkSettingContentBlock' ), 10, 3 );
			add_action( 'cn_entry_output_content-custom_field-custom_county', array( __CLASS__, 'renderCountyContentBlock' ), 10, 3 );
			add_action( 'cn_entry_output_content-custom_field-custom_chapter', array( __CLASS__, 'renderChapterContentBlock' ), 10, 3 );
			add_action( 'cn_entry_output_content-custom_field-special_interest', array( __CLASS__, 'renderSpecialInterestContentBlock' ), 10, 3 );
			add_action( 'cn_entry_output_content-custom_field-custom_ancc', array( __CLASS__, 'renderCertificationsContentBlock' ), 10, 3 );
		}

		public static function removeAAPPNFilters() {

			remove_filter( 'cn_address_options', array( __CLASS__, 'removeAAPPNAddressOption' ) );
			remove_filter( 'cn_phone_options', array( __CLASS__, 'removeAAPPNPhoneOption' ) );
			remove_filter( 'cn_email_options', array( __CLASS__, 'removeAAPPNEmailOption' ) );

			remove_filter( 'cn_addresses', array( __CLASS__, 'removeAAPPNAddresses' ) );
			remove_filter( 'cn_phone_numbers', array( __CLASS__, 'removeAAPPNPhone' ) );
			remove_filter( 'cn_email_addresses', array( __CLASS__, 'removeAAPPNEmail' ) );
		}

		public static function registerMetaboxes() {

			$atts = array(
				'title'    => 'Work Setting Options', // Change this to a name which applies to your project.
				'id'       => 'custom_work_setting', // Change this so it is unique to you project.
				'context'  => 'normal',
				'priority' => 'core',
				'fields'   => array(
					array(
						'name'       => 'Work Setting',     // Change this field name to something which applies to you project.
						'show_label' => TRUE,             // Whether or not to display the 'name'. Changing it to false will suppress the name.
						'id'         => 'id_work_setting', // Change this so it is unique to you project. Each field id MUST be unique.
						'type'       => 'checkboxgroup',  // This is the field type being added.
						'options'    => self::getWorkSettingOptions(),
						'default'    => 'Private Practice', // This is the default selected option. Leave blank for none.
					),
				),
			);

			cnMetaboxAPI::add( $atts );

			$atts = array(
				'title'    => 'County Location', // Change this to a name which applies to your project.
				'id'       => 'custom_county', // Change this so it is unique to you project.
				'context'  => 'normal',
				'priority' => 'core',
				'fields'   => array(
					array(
						'name'       => 'County',     // Change this field name to something which applies to you project.
						'show_label' => TRUE,             // Whether or not to display the 'name'. Changing it to false will suppress the name.
						'id'         => 'id_county', // Change this so it is unique to you project. Each field id MUST be unique.
						'type'       => 'select',  // This is the field type being added.
						'options'    => self::getCountyOptions(),
						'default'    => '', // This is the default selected option. Leave blank for none.
					),
				),
			);

			cnMetaboxAPI::add( $atts );

			$atts = array(
				'title'    => 'Special Interest Group', // Change this to a name which applies to your project.
				'id'       => 'special_interest',   // Change this so it is unique to you project.
				'context'  => 'normal',
				'priority' => 'core',
				'fields'   => array(
					array(
						'name'       => 'CAPAPN - child/adolescent',   // Change this field name to something which applies to you project.
						'show_label' => TRUE,           // Whether or not to display the 'name'. Changing it to false will suppress the name.
						'id'         => 'id_special_interest', // Change this so it is unique to you project. Each field id MUST be unique.
						'type'       => 'checkbox',     // This is the field type being added.
					),
				),
			);

			cnMetaboxAPI::add( $atts );

			$atts = array(
				'title'    => 'Chapter', // Change this to a name which applies to your project.
				'id'       => 'custom_chapter', // Change this so it is unique to you project.
				'context'  => 'normal',
				'priority' => 'core',
				'fields'   => array(
					array(
						'name'       => 'Chapter',     // Change this field name to something which applies to you project.
						'show_label' => TRUE,             // Whether or not to display the 'name'. Changing it to false will suppress the name.
						'id'         => 'id_chapter', // Change this so it is unique to you project. Each field id MUST be unique.
						'type'       => 'checkboxgroup',  // This is the field type being added.
						'options'    => self::getChapterOptions(),
						'default'    => '', // This is the default selected option. Leave blank for none.
					),
				),
			);

			cnMetaboxAPI::add( $atts );

			$atts = array(
				'title'    => 'Preferred first name/nickname',         // Change this to a name which applies to your project.
				'id'       => 'custom_preferred_name',           // Change this so it is unique to you project.
				'context'  => 'normal',
				'priority' => 'core',
				'fields'   => array(
					array(
						'name'       => 'Preferred first name/nickname', // Change this field name to something which applies to you project.
						'show_label' => TRUE,         // Whether or not to display the 'name'. Changing it to false will suppress the name.
						'id'         => 'id_preferred_name',   // Change this so it is unique to you project. Each field id MUST be unique.
						'type'       => 'text',       // This is the field type being added.
						'size'       => 'regular',    // This can be changed to one of the following: 'small', 'regular', 'large'
					),
				),
			);

			cnMetaboxAPI::add( $atts );

			$atts = array(
				'title'    => 'Do you accept Insurance?', // Change this to a name which applies to your project.
				'id'       => 'accept_insurance',   // Change this so it is unique to you project.
				'context'  => 'normal',
				'priority' => 'core',
				'fields'   => array(
					array(
						'name'       => 'Yes, I accept insurance.',   // Change this field name to something which applies to you project.
						'show_label' => TRUE,           // Whether or not to display the 'name'. Changing it to false will suppress the name.
						'id'         => 'id_accept_insurance', // Change this so it is unique to you project. Each field id MUST be unique.
						'type'       => 'checkbox',     // This is the field type being added.
					),
				),
			);

			cnMetaboxAPI::add( $atts );

			$atts = array(
				'title'    => 'Membership Status',         // Change this to a name which applies to your project.
				'id'       => 'expiration_date_metabox', // Change this so it is unique to you project.
				'context'  => 'normal',
				'priority' => 'core',
				'fields'   => array(
					array(
						'name'       => 'Membership Active Until',            // Change this field name to something which applies to you project.
						'show_label' => TRUE,              // Whether or not to display the 'name'. Changing it to false will suppress the name.
						'id'         => 'expiration_date', // Change this so it is unique to you project. Each field id MUST be unique.
						'type'       => 'datepicker',      // This is the field type being added.
						'readonly'   => is_admin() && ! current_user_can( 'manage_options' ) ? TRUE : FALSE
					),
				),
			);

			cnMetaboxAPI::add( $atts );

			$atts = array(
				'title'    => 'ANCC Certification', // Change this to a name which applies to your project.
				'id'       => 'custom_ancc', // Change this so it is unique to you project.
				'context'  => 'normal',
				'priority' => 'core',
				'fields'   => array(
					array(
						'name'       => 'ANCC Certification',     // Change this field name to something which applies to you project.
						'show_label' => TRUE,             // Whether or not to display the 'name'. Changing it to false will suppress the name.
						'id'         => 'id_ancc', // Change this so it is unique to you project. Each field id MUST be unique.
						'type'       => 'checkboxgroup',  // This is the field type being added.
						'options'    => self::getCertificationOptions(),
						'default'    => '', // This is the default selected option. Leave blank for none.
					),
				),
			);

			cnMetaboxAPI::add( $atts );
		}

		public static function csvImportOptions( $fields ) {

			$fields['id_work_setting']     = 'Work Setting';
			$fields['id_special_interest'] = 'Special Interest Group';
			$fields['id_chapter']          = 'Chapter';
			$fields['id_ancc']             = 'ANCC Certification';

			return $fields;
		}

		/**
		 * @param int         $id
		 * @param array       $row
		 * @param cnCSV_Entry $entry
		 */
		public static function csvImportCustomFields( $id, $row, $entry ) {

			$data = array();

			if ( $entry->arrayKeyExists( $row, 'id_work_setting' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'id_work_setting', '' );
				$value = cnFormatting::maybeJSONdecode( stripslashes( $value ) );

				$data[] = array(
					'key'   => 'id_work_setting',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'id_special_interest' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'id_special_interest', '' );
				$value = cnFormatting::maybeJSONdecode( stripslashes( $value ) );

				$data[] = array(
					'key'   => 'id_special_interest',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'id_chapter' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'id_chapter', '' );
				$value = cnFormatting::maybeJSONdecode( stripslashes( $value ) );

				$data[] = array(
					'key'   => 'id_chapter',
					'value' => $value,
				);
			}

			if ( $entry->arrayKeyExists( $row, 'id_ancc' ) ) {

				// The field_id should match exactly the field id used when registering the custom field.
				$value = $entry->arrayPull( $row, 'id_ancc', '' );
				$value = cnFormatting::maybeJSONdecode( stripslashes( $value ) );

				$data[] = array(
					'key'   => 'id_ancc',
					'value' => $value,
				);
			}

			if ( 0 < count( $data ) ) {

				cnEntry_Action::meta( 'update', $id, $data );
			}
		}

		public static function csvExportFieldsConfig( $fields ) {

			$fields[] = array(
				'field'  => 'id_work_setting',
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'id_special_interest',
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'id_chapter',
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'id_preferred_name',
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'id_accept_insurance',
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'expiration_date',
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			$fields[] = array(
				'field'  => 'id_ancc',
				'type'   => 5,
				'fields' => '',
				'table'  => CN_ENTRY_TABLE_META,
				'types'  => NULL,
			);

			return $fields;
		}

		public static function csvExportHeaderNames( $fields ) {

			$fields['id_work_setting']     = 'Work Setting';
			$fields['id_special_interest'] = 'Special Interest Group';
			$fields['id_chapter']          = 'Chapter';
			$fields['id_preferred_name']   = 'Preferred first name/nickname';
			$fields['id_accept_insurance'] = 'Accept Insurance?';
			$fields['expiration_date']     = 'Expiration Date';
			$fields['id_ancc']             = 'ANCC Certification';

			return $fields;
		}

		private static function arrayKeys( array $array ) {

			$keys = array();

			foreach ( $array as $item ) {

				$keys[] = str_replace( '-', '_', cnFormatting::sanitizeStringStrong( $item ) );
			}

			return array_combine( $keys, $array );
		}

		private static function getWorkSettingOptions() {

			$options = array(
				'Private Practice',
				'Community Mental Health',
				'Hospital',
				'Other',
			);

			return self::arrayKeys( $options );
		}

		private static function getWorkSettingChoice( $key ) {

			$counties = self::getWorkSettingOptions();

			if ( isset( $counties[ $key ] ) ) {

				return $counties[ $key ];
			}

			return FALSE;
		}

		private static function getCountyOptions() {

			$options = array(
				'Adams, WA',
				'Asotin, WA',
				'Benton, WA',
				'British Columbia, Canada',
				'Chelan, WA',
				'Clallam, WA',
				'Clark, WA',
				'Columbia, WA',
				'Cowlitz, WA',
				'Douglas, WA',
				'Ferry, WA',
				'Franklin, WA',
				'Garfield, WA',
				'Grant, WA',
				'Grays Harbor, WA',
				'Island, WA',
				'Jefferson, WA',
				'King, WA',
				'Kitsap, WA',
				'Kittitas, WA',
				'Klickitat, WA',
				'Lewis, WA',
				'Lincoln, WA',
				'Mason, WA',
				'Multnomah, OR',
				'Okanogan, WA',
				'Pacific, WA',
				'Pend Oreille, WA',
				'Pierce, WA',
				'San Juan, WA',
				'Skagit, WA',
				'Skamania, WA',
				'Snohomish, WA',
				'Spokane, WA',
				'Stevens, WA',
				'Thurston, WA',
				'Wahkiakum, WA',
				'Walla Walla, WA',
				'Whatcom, WA',
				'Whitman, WA',
				'Yakima, WA',
				'Other',
			);

			return self::arrayKeys( $options );
		}

		private static function getCountyChoice( $key ) {

			$counties = self::getCountyOptions();

			if ( isset( $counties[ $key ] ) ) {

				return $counties[ $key ];
			}

			return FALSE;
		}

		private static function getChapterOptions() {

			$options = array(
				'At Large',
				'East King County',
				'Olympia',
				'Peninsula',
				'Portland',
				'Seattle',
				'Spokane',
				'Tacoma',
				'Vancouver',
				'Whatcom/Skagit',
			);

			return self::arrayKeys( $options );
		}

		private static function getChapterChoice( $key ) {

			$counties = self::getChapterOptions();

			if ( isset( $counties[ $key ] ) ) {

				return $counties[ $key ];
			}

			return FALSE;
		}

		private static function getCertificationOptions() {

			return array(
				'adult_practitioner'  => 'Adult Psychiatric and Mental Health Nurse Practitioner',
				'family_practitioner' => 'Family Psychiatric and Mental Health Nurse Practitioner',
				'cs_adult'            => 'Clinical Specialist in Adult Psychiatric and Mental Health Nursing',
				'cs_ped'              => 'Clinical Specialist in Child and/or Adolescent Psychiatric and Mental Health Nursing',
				'student'             => 'Student',
				'other'               => 'Other-An AAPPN representative will contact you',
			);
		}

		private static function getCertificationChoice( $key ) {

			$counties = self::getCertificationOptions();

			if ( isset( $counties[ $key ] ) ) {

				return $counties[ $key ];
			}

			return FALSE;
		}

		public static function multipleCategorySearchOptions( $select ) {

			//$user = wp_get_current_user();

			foreach ( $select as $key => $options ) {

				if ( 77 == $options[0] ) {

					if ( ! current_user_can( 'read' ) ) unset( $select[ $key ] );

					//if ( in_array( 'subscriber', (array) $user->roles ) ) {
					//unset( $select[ $key ] );
					//}

				}
			}

			return $select;
		}

		public static function removeMetaboxes() {

			cnMetaboxAPI::remove( 'metabox-logo' );
			cnMetaboxAPI::remove( 'metabox-messenger' );
			cnMetaboxAPI::remove( 'metabox-social-media' );
			cnMetaboxAPI::remove( 'metabox-logo' );
			cnMetaboxAPI::remove( 'metabox-note' );
			cnMetaboxAPI::remove( 'metabox-date' );
			cnMetaboxAPI::remove( 'metabox-meta' );
		}

		public static function entryTypes( $atts ) {

			$atts['entry_type'] = array( __( 'Individual', 'connections' ) => 'individual' );

			$atts['default']['type'] = 'individual';

			$atts['default']['visibility'] = 'public';

			return $atts;
		}

		public static function registerAddressTypes( $options ) {

			$options = array(
				'work'           => 'Address',
				'work_secondary' => 'Secondary Address',
				'aappn_mailing'  => 'For AAPPN Use: Mailing Address',
				'other'          => 'Other',
			);

			return $options;
		}

		public static function registerPhoneNumberTypes( $options ) {

			$options = array(
				'primary_work'             => 'Phone',
				'alt_primary_work'         => 'Alternate Phone',
				'secondary_work_phone'     => 'Secondary Phone',
				'alt_secondary_work_phone' => 'Alternate Secondary Phone',
				'primary_work_fax'         => 'Fax',
				'secondary_work_fax'       => 'Secondary Fax',
				'aappn_daytime_phone'      => 'For AAPPN Use: Day-time Phone',
				'aappn_alt_daytime_phone'  => 'For AAPPN Use: Alternate Day-time Phone',
			);

			return $options;
		}

		public static function registerEmailAddressTypes( $options ) {

			$options = array(
				'work'           => 'Primary Email',
				'secondary_work' => 'Secondary Email',
				'personal'       => 'For AAPPN Use: Contact Email',
			);

			return $options;
		}


		public static function removeAAPPNAddressOption( $options ) {

			if ( /*is_admin() &&*/ current_user_can( 'connections_view_unlisted' ) ) return $options;

			if ( isset( $options['aappn_mailing'] ) ) unset( $options['aappn_mailing'] );

			return $options;
		}

		public static function removeAAPPNPhoneOption( $options ) {

			if ( /*is_admin() &&*/ current_user_can( 'connections_view_unlisted' ) ) return $options;

			if ( isset( $options['aappn_daytime_phone'] ) ) unset( $options['aappn_daytime_phone'] );
			if ( isset( $options['aappn_alt_daytime_phone'] ) ) unset( $options['aappn_alt_daytime_phone'] );

			return $options;
		}

		public static function removeAAPPNEmailOption( $options ) {

			if ( /*is_admin() &&*/ current_user_can( 'connections_view_unlisted' ) ) return $options;

			if ( isset( $options['personal'] ) ) unset( $options['personal'] );

			return $options;
		}

		public static function setAAPPNAddressUnlisted( $address ) {

			//if ( is_admin() && current_user_can( 'connections_view_unlisted' ) ) return $address;

			if ( is_array( $address ) && 'aappn_mailing' == $address['type'] ) {

				$address['visibility'] = 'unlisted';
			}

			if ( is_object( $address ) && 'aappn_mailing' == $address->type ) {

				$address->visibility = 'unlisted';
			}

			return $address;
		}

		public static function setAAPPNPhoneUnlisted( $phone ) {

			//if ( is_admin() && current_user_can( 'connections_view_unlisted' ) ) return $phone;

			if ( is_array( $phone ) && ( 'aappn_daytime_phone' == $phone['type'] || 'aappn_alt_daytime_phone' == $phone['type'] ) ) {

				$phone['visibility'] = 'unlisted';
			}

			if ( is_object( $phone ) && ( 'aappn_daytime_phone' == $phone->type || 'aappn_alt_daytime_phone' == $phone->type ) ) {

				$phone->visibility = 'unlisted';
			}

			return $phone;
		}

		public static function setAAPPNEmailUnlisted( $email ) {

			//if ( is_admin() && current_user_can( 'connections_view_unlisted' ) ) return $email;

			if ( is_array( $email ) && 'personal' == $email['type'] ) {

				$email['visibility'] = 'unlisted';
			}

			if ( is_object( $email ) && 'personal' == $email->type ) {

				$email->visibility = 'unlisted';
			}

			return $email;
		}

		public static function removeAAPPNAddresses( $addresses ) {

			if ( is_admin() && current_user_can( 'connections_view_unlisted' ) ) return $addresses;

			foreach ( $addresses as $key => $address ) {

				if ( 'aappn_mailing' == $address->type ) {

					unset( $addresses[ $key ] );
				}
			}

			return $addresses;
		}

		public static function removeAAPPNPhone( $numbers ) {

			if ( is_admin() && current_user_can( 'connections_view_unlisted' ) ) return $numbers;

			foreach ( $numbers as $key => $phone ) {

				if ( 'aappn_daytime_phone' == $phone->type || 'aappn_alt_daytime_phone' == $phone->type ) {

					unset( $numbers[ $key ] );
				}
			}

			return $numbers;
		}

		public static function removeAAPPNEmail( $addresses ) {

			if ( is_admin() && current_user_can( 'connections_view_unlisted' ) ) return $addresses;

			foreach ( $addresses as $key => $address ) {

				if ( 'personal' == $address->type ) {

					unset( $addresses[ $key ] );
				}
			}

			return $addresses;
		}

		public static function registerContentBlocks( $blocks ) {

			$blocks['entry_field-primary']              = 'Primary Contact Details';
			$blocks['entry_field-secondary']            = 'Secondary Contact Details';
			$blocks['entry_field-bio']                  = 'Practice Description';
			$blocks['custom_field-preferred_name']      = 'Preferred First Name/Nickname';
			$blocks['categories-age_groups']            = 'Age Groups';
			$blocks['categories-issues_treated']        = 'Issues Treated';
			$blocks['categories-modality']              = 'Modality';
			$blocks['custom_field-accept_insurance']    = 'I Accept Insurance';
			$blocks['custom_field-custom_work_setting'] = 'Work Setting';
			$blocks['custom_field-custom_county']       = 'County';
			$blocks['custom_field-custom_chapter']      = 'Chapter';
			$blocks['custom_field-special_interest']    = 'Special Interest Group';
			$blocks['custom_field-custom_ancc']         = 'ANCC Certification';

			return $blocks;
		}

		/**
		 * @param cnOutput   $entry
		 * @param array      $shortcode_atts
		 * @param cnTemplate $template
		 */
		public static function renderPrimaryContentBlock( $entry, $shortcode_atts, $template ) {

			$entry->getAddressBlock( array( 'type' => 'work' ) );
			$entry->getPhoneNumberBlock( array( 'type' => array( 'primary_work', 'alt_primary_work', 'primary_work_fax' ) ) );
			$entry->getEmailAddressBlock( array( 'type' => 'work') );
			$entry->getLinkBlock( array( 'type' => 'website' ) );
		}

		/**
		 * @param cnOutput   $entry
		 * @param array      $shortcode_atts
		 * @param cnTemplate $template
		 */
		public static function renderSecondaryContentBlock( $entry, $shortcode_atts, $template ) {

			$entry->getAddressBlock( array( 'type' => 'work_secondary' ) );
			$entry->getPhoneNumberBlock( array( 'type' => array( 'secondary_work_phone', 'alt_secondary_work_phone', 'secondary_work_fax' ) ) );
			$entry->getEmailAddressBlock( array( 'type' => 'secondary_work') );
			$entry->getLinkBlock( array( 'type' => 'blog' ) );
		}

		/**
		 * @param cnOutput   $entry
		 * @param array      $shortcode_atts
		 * @param cnTemplate $template
		 */
		public static function renderBioFieldContentBlock( $entry, $shortcode_atts, $template ) {

			$entry->getBioBlock();
		}

		/**
		 * @param cnOutput   $entry
		 * @param array      $shortcode_atts
		 * @param cnTemplate $template
		 */
		public static function renderPreferredNameContentBlock( $entry, $shortcode_atts, $template ) {

			if ( ! is_user_logged_in() ) return;

			$name = $entry->getMeta( array( 'key' => 'id_preferred_name', 'single' => TRUE ) );

			if ( 0 < strlen( $name ) ) {

				echo '<p>' . esc_html( $name ) . '</p>';
			}
		}

		/**
		 * @param cnOutput   $entry
		 * @param array      $shortcode_atts
		 * @param cnTemplate $template
		 */
		public static function renderAgeGroupsContentBlock( $entry, $shortcode_atts, $template ) {

			$out      = array();
			$term_ids = wp_list_pluck( $entry->getCategory(), 'term_id' );

			if ( ! empty( $term_ids ) ) {

				$atts = array(
					'child_of' => 2,
					'include'  => $term_ids,
				);

				$terms = cnTerm::getTaxonomyTerms( 'category', $atts );

				if ( ! is_wp_error( $terms ) && ! empty( $terms ) ) {

					foreach ( $terms as $term ) {

						$out[] = esc_html( $term->name );
					}

					if ( ! empty( $out ) ) echo '<p>' . implode( ', ', $out ) . '</p>';
				}
			}
		}

		/**
		 * @param cnOutput   $entry
		 * @param array      $shortcode_atts
		 * @param cnTemplate $template
		 */
		public static function renderIssuesTreatedContentBlock( $entry, $shortcode_atts, $template ) {

			$out      = array();
			$term_ids = wp_list_pluck( $entry->getCategory(), 'term_id' );

			if ( ! empty( $term_ids ) ) {

				$atts = array(
					'child_of' => 105,
					'include'  => $term_ids,
				);

				$terms = cnTerm::getTaxonomyTerms( 'category', $atts );

				if ( ! is_wp_error( $terms ) && ! empty( $terms ) ) {

					foreach ( $terms as $term ) {

						$out[] = esc_html( $term->name );
					}

					if ( ! empty( $out ) ) echo '<p>' . implode( ', ', $out ) . '</p>';
				}
			}
		}

		/**
		 * @param cnOutput   $entry
		 * @param array      $shortcode_atts
		 * @param cnTemplate $template
		 */
		public static function renderModalityContentBlock( $entry, $shortcode_atts, $template ) {

			$out      = array();
			$term_ids = wp_list_pluck( $entry->getCategory(), 'term_id' );

			if ( ! empty( $term_ids ) ) {

				$atts = array(
					'child_of' => 77,
					'include'  => $term_ids,
				);

				$terms = cnTerm::getTaxonomyTerms( 'category', $atts );

				if ( ! is_wp_error( $terms ) && ! empty( $terms ) ) {

					foreach ( $terms as $term ) {

						$out[] = esc_html( $term->name );
					}

					if ( ! empty( $out ) ) echo '<p>' . implode( ', ', $out ) . '</p>';
				}
			}
		}

		/**
		 * @param cnOutput   $entry
		 * @param array      $shortcode_atts
		 * @param cnTemplate $template
		 */
		public static function renderAcceptInsuranceContentBlock( $entry, $shortcode_atts, $template ) {

			$value = $entry->getMeta( array( 'key' => 'id_accept_insurance', 'single' => TRUE ) );

			cnFormatting::toBoolean( $value );

			if ( $value ) echo '<p>' . esc_html( cnFormatting::toYesNo( $value ) ) . '</p>';
		}

		/**
		 * @param cnOutput   $entry
		 * @param array      $shortcode_atts
		 * @param cnTemplate $template
		 */
		public static function renderWorkSettingContentBlock( $entry, $shortcode_atts, $template ) {

			if ( ! is_user_logged_in() ) return;

			$out   = array();
			$value = $entry->getMeta( array( 'key' => 'id_work_setting', 'single' => TRUE ) );

			if ( ! empty( $value ) ) {

				foreach ( $value as $item ) {

					if ( $choice = self::getWorkSettingChoice( $item ) ) $out[] = esc_html( $choice );
				}

				if ( ! empty( $out ) )  echo '<p>' . implode( ', ', $out ) . '</p>';
			}
		}

		/**
		 * @param cnOutput   $entry
		 * @param array      $shortcode_atts
		 * @param cnTemplate $template
		 */
		public static function renderCountyContentBlock( $entry, $shortcode_atts, $template ) {

			if ( ! is_user_logged_in() ) return;

			$value = $entry->getMeta( array( 'key' => 'id_county', 'single' => TRUE ) );

			if ( ! empty( $value ) ) {

				if ( $choice = self::getCountyChoice( $value ) ) {

					echo '<p>' . esc_html( $choice ) . '</p>';
				}
			}
		}

		/**
		 * @param cnOutput   $entry
		 * @param array      $shortcode_atts
		 * @param cnTemplate $template
		 */
		public static function renderChapterContentBlock( $entry, $shortcode_atts, $template ) {

			if ( ! is_user_logged_in() ) return;

			$value = $entry->getMeta( array( 'key' => 'id_chapter', 'single' => TRUE ) );

			if ( ! empty( $value ) ) {

				if ( is_array( $value ) ) {

					foreach ( $value as $county ) {

						if ( $choice = self::getChapterChoice( $county ) ) {

							echo '<p>' . esc_html( $choice ) . '</p>';
						}
					}

				} elseif ( is_string( $value ) ) {

					if ( $choice = self::getChapterChoice( $value ) ) {

						echo '<p>' . esc_html( $choice ) . '</p>';
					}
				}
			}
		}

		/**
		 * @param cnOutput   $entry
		 * @param array      $shortcode_atts
		 * @param cnTemplate $template
		 */
		public static function renderSpecialInterestContentBlock( $entry, $shortcode_atts, $template ) {

			if ( ! is_user_logged_in() ) return;

			$value = $entry->getMeta( array( 'key' => 'id_special_interest', 'single' => TRUE ) );

			cnFormatting::toBoolean( $value );

			if ( $value ) {

				echo '<p>CAPAPN - child/adolescent</p>';
			}
		}

		/**
		 * @param cnOutput   $entry
		 * @param array      $shortcode_atts
		 * @param cnTemplate $template
		 */
		public static function renderCertificationsContentBlock( $entry, $shortcode_atts, $template ) {

			if ( ! is_user_logged_in() ) return;

			$out   = array();
			$value = $entry->getMeta( array( 'key' => 'id_ancc', 'single' => TRUE ) );

			if ( ! empty( $value ) && is_array( $value ) ) {

				foreach ( $value as $item ) {

					if ( $choice = self::getCertificationChoice( $item ) ) $out[] = esc_html( $choice );
				}

				if ( ! empty( $out ) ) echo '<p>' . implode( ', ', $out ) . '</p>';
			}
		}

		/**
		 * @param cnOutput   $entry
		 * @param array      $shortcode_atts
		 * @param cnTemplate $template
		 */
		public static function renderCustomBlock( $entry, $shortcode_atts, $template ) {

			echo '<p>content here</p>';
		}
	}

	/**
	 * Start up the extension.
	 *
	 * @access                public
	 * @since                 1.0
	 * @return mixed (object)|(bool)
	 */
	function Connections_AAPPN() {

		if ( class_exists( 'connectionsLoad' ) ) {

			return Connections_AAPPN::instance();

		} else {

			add_action(
				'admin_notices',
				create_function(
					'',
					'echo \'<div id="message" class="error"><p><strong>ERROR:</strong> Connections must be installed and active in order use the Connections AAPPN Extension.</p></div>\';'
				)
			);

			return FALSE;
		}
	}

	/**
	 * We'll load the extension on `plugins_loaded` so we know Connections will be loaded and ready first.
	 */
	add_action( 'plugins_loaded', 'Connections_AAPPN' );

endif;
